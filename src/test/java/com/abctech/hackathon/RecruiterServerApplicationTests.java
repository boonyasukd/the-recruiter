package com.abctech.hackathon;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import com.abctech.hackathon.entity.Department;
import com.abctech.hackathon.entity.Job;
import com.abctech.hackathon.entity.JobLevel;
import com.abctech.hackathon.service.JobService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.Map;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RecruiterServerApplication.class)
@WebAppConfiguration
@Transactional
public class RecruiterServerApplicationTests {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;

    @Autowired
    private JobService jobService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

//    @Test(expected = DataAccessException.class)
//    public void contextLoads() {
//        recruiterService.findApplicantByEmail("john.smith@gmail.com");
//    }
//
//    @Test
//    public void contextLoads2() {
//        List<Application> result = recruiterService.findApplicationByEmail("john.smith@gmail.com");
//        assertThat(result.size(), equalTo(0));
//    }

    @Test
    public void testFindApplicantByName() throws Exception {
        mockMvc.perform(get("/api/recruiter/applicants/search")
                        .param("firstName", "john")
                        .param("lastName", "smith"))
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("[]"));
    }

    @Test
    public void testFindApplicantByEmail() throws Exception {
        mockMvc.perform(get("/api/recruiter/applicants/search").param("email", "john.smith@gmail.com"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void testAddJob() throws Exception {
        Map<String, String> params = new HashMap<>();
        Job job1 = new Job();
        job1.setName("Senior Java Dev");
        job1.setDescription("Test01");
        job1.setLevel(JobLevel.SENIOR);
        job1.setDepartment(Department.AMEDIA);
        jobService.createOrUpdateJob(job1);

        assertThat(jobService.getJobs(params).size(), not(equalTo(0)));

        Job job2 = new Job();
        job2.setName("Lead Dev");
        job2.setDescription("Test02");
        job2.setLevel(JobLevel.LEAD);
        job2.setDepartment(Department.AMEDIA);
        jobService.createOrUpdateJob(job2);

        assertThat(jobService.getJobs(params).size(), not(equalTo(1)));
    }
}
