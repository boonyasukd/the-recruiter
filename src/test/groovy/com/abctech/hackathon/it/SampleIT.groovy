package com.abctech.hackathon.it

import com.fasterxml.jackson.databind.JsonNode
import groovy.util.logging.Slf4j
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test

import static com.jayway.restassured.RestAssured.*
import static com.jayway.restassured.matcher.RestAssuredMatchers.*
import static org.hamcrest.Matchers.*
import static org.junit.Assert.assertThat;

@Slf4j
class SampleIT {
    static def cfg
    static def namespace
    static def perNamespace
    static def fileName
    static def ready

    @BeforeClass
    static void initialize() {
        cfg = this.getClass().getResource('/kubernetes/build').file
        namespace = "${System.properties.'appName'}-${System.currentTimeMillis()}"
        perNamespace = "--namespace=${namespace}"
        fileName = "/tmp/namespace-${namespace}.yml"

        manageNamespace('create')
        manageResourcesNoWait('create')
        Thread.sleep(1000 * 60 * 1)

        ready = arePodsReady();
        println "are all pods ready: ${ready}"
    }

    @AfterClass
    static void tearDown() {
        manageResources('delete')
        Thread.sleep(1000 * 60 * 1)
        manageNamespace('delete')
    }

    private static void manageNamespace(def action) {
        if (action == 'create') {
            new File(fileName) << /{ "kind": "Namespace", "apiVersion": "v1", "metadata": { "name": "${namespace}" } }/
            execute("/kubernetes/kubectl ${action} -f ${fileName}")
            execute("rm ${fileName}")        
        } else {
            execute("/kubernetes/kubectl delete namespaces ${namespace}")
            def ids = execute("docker ps -a -q -f status=exited").join(' ')
            execute("docker rm -v ${ids}")
            //execute("docker rmi ${System.properties.'imageName'}")
        }
    }

    private static void manageResources(def action) {
        execute("/kubernetes/kubectl ${action} -f ${cfg} ${perNamespace}")
    }

    private static void manageResourcesNoWait(def action) {
        println "/kubernetes/kubectl ${action} -f ${cfg} ${perNamespace}".execute().text
    }

    private static String[] arePodsReady() {
        def result = execute("/kubernetes/kubectl get pods ${perNamespace}")
        if (result.size() == 0) { 
            return false
        } else {
            return result[0].split()[0] == 'NAME' && result[1..-1].every { it.split()[2] == 'Running' }
        }
    }

    private static String[] execute(def cmd) {
        println "\$ > ${cmd}"
        def proc = cmd.execute();
        def outputStream = new StringBuilder();
        proc.waitForProcessOutput(outputStream, System.err)
        def result = outputStream.toString()
        println result

        return (result.size() == 0) ? new String[0] : result.split('\\n')
    }

    private static String execute2(def cmd) {
        println "\$ > ${cmd}"
        def proc = cmd.execute();
        def outputStream = new StringBuilder();
        proc.waitForProcessOutput(outputStream, System.err)
        def result = outputStream.toString()
        println result
        
        return result
    }

    @Before
    void beforeEach() {

    }

    @After
    void afterEach() {

    }

    @Test(expected = IndexOutOfBoundsException.class)
    void indexOutOfBoundsAccess() {
        [1,2,3,4].get(4)
    }

    @Test
    void findJobTest() {
        if (ready) {
            def endpoint = "http://recruiter-svc.${namespace}.svc.cluster.local:8080/api/recruiter/jobs/search"
            JsonNode result = 
                given()
                        .log().all()
                        .baseUri(endpoint)
                        .queryParam("dept", "AMEDIA")
                        .queryParam("jlevel", "SENIOR").
                then()
                        .expect().statusCode(200).
                when()
                        .get()
                        .andReturn().body().as(JsonNode.class);

            assertThat(result.isArray(), equalTo(true))
            assertThat(result.size(), equalTo(3))
        }
    }
}

