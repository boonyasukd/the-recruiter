package com.abctech.hackathon.web.controller;

import com.abctech.hackathon.entity.Applicant;
import com.abctech.hackathon.entity.Application;
import com.abctech.hackathon.entity.Job;
import com.abctech.hackathon.entity.JobVacancy;
import com.abctech.hackathon.service.ApplicantService;
import com.abctech.hackathon.service.ApplicationService;
import com.abctech.hackathon.service.JobService;
import com.abctech.hackathon.service.JobVacancyService;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.*;

/**
 *
 * @author b0nyb0y
 */
@CrossOrigin(maxAge = 3600)
@RestController
public class MainController {

    @Autowired
    private ApplicantService applicantService;

    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private JobService jobService;

    @Autowired
    private JobVacancyService jobVacancyService;

    @JsonView(View.Applicant.class)
    @RequestMapping(path = "/api/recruiter/applicants/create", method = POST)
    public Applicant createApplicant(@Valid @RequestBody Applicant applicant) {
        return applicantService.createOrUpdateApplicant(applicant);
    }

    @JsonView(View.Applicant.class)
    @RequestMapping(path = "/api/recruiter/applicants/search", method = GET)
    public List<Applicant> findApplicantsByCriteria(@RequestParam Map<String, String> params) {
        return applicantService.findApplicantsByCriteria(params);
    }

    @JsonView(View.Vacancy.class)
    @RequestMapping(path = "/api/recruiter/applications/create", method = POST)
    public Application createApplication(@Valid @RequestBody Application application) {
        return applicationService.createApplication(application);
    }

    @RequestMapping(path = "/api/recruiter/jobs/create", method = POST)
    public Job createJob(@Valid @RequestBody Job job) {
        return jobService.createOrUpdateJob(job);
    }

    @RequestMapping(path = "/api/recruiter/jobs/search", method = GET)
    public List<Job> findJobsByCriteria(@RequestParam Map<String, String> params) {
        return jobService.getJobs(params);
    }

    @JsonView(View.Vacancy.class)
    @RequestMapping(path = "/api/recruiter/vacancies/create", method = POST)
    public JobVacancy createJobVacancy(@Valid @RequestBody JobVacancy jobVacancy) {
        return jobVacancyService.createOrUpdateJob(jobVacancy);
    }

    @JsonView(View.Vacancy.class)
    @RequestMapping(path = "/api/recruiter/vacancies/search", method = GET)
    public List<JobVacancy> findJobVacanciesByCriteria(@RequestParam Map<String, String> params) {
        return jobVacancyService.getAllJobVacancy(params);
    }
}
