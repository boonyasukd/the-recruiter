package com.abctech.hackathon.web;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.abctech.hackathon.exception.InternalErrorException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author b0nyb0y
 */
@ControllerAdvice
public class WebExceptionHandler {
    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ExceptionHandler(InternalErrorException.class)
    public void handleNotFound() {
        // Nothing to do
    }
}
