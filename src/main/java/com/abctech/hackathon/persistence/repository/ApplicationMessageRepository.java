package com.abctech.hackathon.persistence.repository;

import com.abctech.hackathon.entity.ApplicationMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.transaction.annotation.Propagation.MANDATORY;

/**
 * Created by Tai_laptop on 12/29/2015.
 */
@Repository
@Transactional(propagation = MANDATORY)
public interface ApplicationMessageRepository extends JpaRepository<ApplicationMessage, Integer>, QueryDslPredicateExecutor {
}
