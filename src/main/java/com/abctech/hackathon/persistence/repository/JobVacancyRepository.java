package com.abctech.hackathon.persistence.repository;

import com.abctech.hackathon.entity.JobVacancy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.transaction.annotation.Propagation.MANDATORY;

/**
 *
 * @author b0nyb0y
 */
@Repository
@Transactional(propagation = MANDATORY)
public interface JobVacancyRepository extends JpaRepository<JobVacancy, Integer>, QueryDslPredicateExecutor {
    
}
