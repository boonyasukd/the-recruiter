package com.abctech.hackathon.persistence.converter;

import java.sql.Time;
import java.time.LocalTime;
import javax.persistence.AttributeConverter;

/**
 *
 * @author b0nyb0y
 */
public class LocalTimeConverter implements AttributeConverter<LocalTime, Time> {

    @Override
    public Time convertToDatabaseColumn(LocalTime x) {
        return (x == null) ? null : Time.valueOf(x);
    }

    @Override
    public LocalTime convertToEntityAttribute(Time y) {
        return (y == null) ? null : y.toLocalTime();
    }
}
