package com.abctech.hackathon.entity;

import com.abctech.hackathon.web.controller.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.hibernate.validator.constraints.Email;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author b0nyb0y
 */
@Entity
@Table(indexes = {
    @Index(columnList = "EMAIL", unique = true),
    @Index(columnList = "FIRSTNAME, LASTNAME", unique = true)
})
public class Applicant extends BaseEntity {
    @NotNull
    @Size(min = 1, max = 32)
    @Column(nullable = false, length = 32)
    private String firstName;

    @NotNull
    @Size(min = 1, max = 32)
    @Column(nullable = false, length = 32)
    private String lastName;

    @NotNull
    @Email
    @Column(nullable = false, length = 64)
    private String email;

    @NotNull
    @Size(min = 9, max = 16)
    @Column(nullable = false, length = 16)
    private String phone;

    @Min(15)
    @Max(120)
    @Column
    private Byte age;

    @JsonView(View.Applicant.class)
    @OneToMany(mappedBy = "applicant")
    private List<Application> applications;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Byte getAge() {
        return age;
    }

    public void setAge(Byte age) {
        this.age = age;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }
}
