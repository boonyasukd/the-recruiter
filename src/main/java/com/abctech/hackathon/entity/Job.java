package com.abctech.hackathon.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static javax.persistence.EnumType.STRING;

/**
 *
 * @author b0nyb0y
 */
@Entity
@Table(indexes = {
    @Index(columnList = "NAME,JLEVEL,DEPARTMENT")
})
public class Job extends BaseEntity {
    @NotNull
    @Column(nullable = false, length = 32)
    private String name;

    @Column
    private String description;

    @NotNull
    @Enumerated(STRING)
    @Column(nullable = false, name = "JLEVEL", length = 16)
    private JobLevel level;

    @NotNull
    @Enumerated(STRING)
    @Column(nullable = false, length = 16)
    private Department department;

    @JsonProperty(access = WRITE_ONLY)
    @OneToMany(mappedBy = "job")
    private List<JobVacancy> jobVacancies;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public JobLevel getLevel() {
        return level;
    }

    public void setLevel(JobLevel level) {
        this.level = level;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public List<JobVacancy> getJobVacancies() {
        return jobVacancies;
    }

    public void setJobVacancies(List<JobVacancy> jobVacancies) {
        this.jobVacancies = jobVacancies;
    }
}
