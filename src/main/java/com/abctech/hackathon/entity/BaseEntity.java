package com.abctech.hackathon.entity;

import static javax.persistence.GenerationType.AUTO;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author b0nyb0y
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = AUTO, generator = "entity_id")
    @SequenceGenerator(name = "entity_id", allocationSize = 1)
    private Integer id;

    @Version
    private Integer version;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (!(this.getClass().isInstance(obj))) {
            return false;
        }

        BaseEntity that = (BaseEntity) obj;
        if (this.id != null && that.id != null) {
            return this.id.equals(that.id);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (id != null) ? id.hashCode() : System.identityHashCode(this);
    }
}
