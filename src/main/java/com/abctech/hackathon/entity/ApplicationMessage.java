package com.abctech.hackathon.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.persistence.annotations.BatchFetch;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import java.time.LocalDateTime;

import static org.eclipse.persistence.annotations.BatchFetchType.IN;
import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;
import static javax.persistence.EnumType.STRING;

/**
 *
 * @author b0nyb0y
 */
@Entity
@Table(indexes = {
    @Index(columnList = "APPLICATION_ID,USER_ID", unique = false)
})
public class ApplicationMessage extends BaseEntity {

    @JsonProperty(access = WRITE_ONLY)
    @NotNull
    @ManyToOne(optional = false)
    @BatchFetch(IN)
    private Application application;

    //@NotNull
    //@ManyToOne(optional = false)
    @ManyToOne
    @BatchFetch(IN)
    private AppUser user;

    @NotNull
    @Column(nullable = false)
    private LocalDateTime lastModified;

    @NotNull
    @Enumerated(STRING)
    @Column(nullable = false, name = "ASTATE", length = 16)
    private ApplicationState state;

    @NotNull
    @Column(nullable = false, length = 512)
    private String comment;

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public AppUser getUser() {
        return user;
    }

    public void setUser(AppUser user) {
        this.user = user;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public ApplicationState getState() {
        return state;
    }

    public void setState(ApplicationState state) {
        this.state = state;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
