package com.abctech.hackathon.entity;

/**
 *
 * @author b0nyb0y
 */
public enum Department {
    AMEDIA("Amedia"),
    STARTSIDEN("Startsiden");

    private String department;

    private Department(String dept){
        this.department = dept;
    }

    public String getDeptName(){
        return department;
    }

}
