package com.abctech.hackathon.entity;

import com.abctech.hackathon.web.controller.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.eclipse.persistence.annotations.BatchFetch;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import static javax.persistence.FetchType.EAGER;
import static org.eclipse.persistence.annotations.BatchFetchType.IN;

/**
 *
 * @author b0nyb0y
 */
@Entity
public class JobVacancy extends BaseEntity {
    @ManyToOne(fetch = EAGER)
    @BatchFetch(IN)
    private Job job;

    @NotNull
    @Column(nullable = false)
    private LocalDate startDate;

    @NotNull
    @Column(nullable = false)
    private LocalDate expireDate;

    @NotNull
    @Column(nullable = false)
    private Boolean active;

    @NotNull
    @Column(nullable = false)
    private Byte totalNo = 0;

    @NotNull
    @Column(nullable = false)
    private Byte remainingNo = 0;

    @JsonView(View.Vacancy.class)
    @OneToMany(mappedBy = "vacancy", fetch = EAGER)
    private List<Application> applications;

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getExpireDate(){
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Byte getTotalNo() {
        return totalNo;
    }

    public void setTotalNo(Byte totalNo) {
        this.totalNo = totalNo;
    }

    public Byte getRemainingNo() {
        return remainingNo;
    }

    public void setRemainingNo(Byte remainingNo) {
        this.remainingNo = remainingNo;
    }

    public List<Application> getApplications() {
        return applications;
    }

    public void setApplications(List<Application> applications) {
        this.applications = applications;
    }
}
