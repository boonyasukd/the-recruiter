package com.abctech.hackathon.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author b0nyb0y
 */
@Entity
@Table(indexes = {
    @Index(columnList = "USERNAME", unique = true)
})
public class AppUser extends BaseEntity {

    @NotNull
    @Column(nullable = false, length = 16)
    private String username;

    @NotNull
    @Column(nullable = false, length = 24)
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
