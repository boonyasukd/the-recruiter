package com.abctech.hackathon.entity;

/**
 *
 * @author b0nyb0y
 */
public enum ApplicationState {
    RECEIVED, REJECTED, BACKGROUND_CHECK, PHONE_INTERVIEW, ONSITE_INTERVIEW, ACCEPTED, DECLINED;
}
