package com.abctech.hackathon.entity;

/**
 *
 * @author b0nyb0y
 */
public enum JobLevel {
    JUNIOR (1),
    INTERMEDIATE (2),
    SENIOR (3),
    LEAD (4),
    DEPARTMENT_HEAD (5);

    private int levelId;

    private JobLevel(int levelId){
        this.levelId = levelId;
    }

    public int getJobLevel(){
        return levelId;
    }
}
