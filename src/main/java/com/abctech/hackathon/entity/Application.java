package com.abctech.hackathon.entity;

import com.abctech.hackathon.web.controller.View;
import com.fasterxml.jackson.annotation.JsonView;
import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.JoinFetch;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.EnumType.STRING;
import static org.eclipse.persistence.annotations.BatchFetchType.IN;

/**
 *
 * @author b0nyb0y
 */
@Entity
public class Application extends BaseEntity {

    @JsonView(View.Vacancy.class)
    @JoinFetch
    @ManyToOne
    private Applicant applicant;

    @JsonView(View.Applicant.class)
    @BatchFetch(IN)
    @ManyToOne(optional = false)
    private JobVacancy vacancy;

    @NotNull
    @Enumerated(STRING)
    @Column(nullable = false, name = "ASTATE", length = 16)
    private ApplicationState state;

    private Integer expectedSalary;

//    @JsonView(View.Vacancy.class)
    @OneToMany(mappedBy = "application", cascade = ALL)
    private List<ApplicationMessage> messages;

    public Applicant getApplicant() {
        return applicant;
    }

    public void setApplicant(Applicant applicant) {
        this.applicant = applicant;
    }

    public JobVacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(JobVacancy vacancy) {
        this.vacancy = vacancy;
    }

    public ApplicationState getState() {
        return state;
    }

    public void setState(ApplicationState state) {
        this.state = state;
    }

    public Integer getExpectedSalary() {
        return expectedSalary;
    }

    public void setExpectedSalary(Integer expectedSalary) {
        this.expectedSalary = expectedSalary;
    }

    public List<ApplicationMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ApplicationMessage> messages) {
        this.messages = messages;
    }
}
