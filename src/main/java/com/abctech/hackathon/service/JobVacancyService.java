package com.abctech.hackathon.service;

import com.abctech.hackathon.entity.Department;
import com.abctech.hackathon.entity.JobVacancy;
import com.abctech.hackathon.persistence.repository.JobVacancyRepository;
import com.mysema.query.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

import static com.abctech.hackathon.entity.QJobVacancy.jobVacancy;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.mysema.query.types.ExpressionUtils.anyOf;

/**
 *
 * @author b0nyb0y
 */
@Service
@Transactional
public class JobVacancyService {

    @Autowired
    private JobVacancyRepository jobVacancyRepo;

    public JobVacancy createOrUpdateJob(JobVacancy jobVacancy) {
        return jobVacancyRepo.saveAndFlush(jobVacancy);
    }

    public void deleteJobVacancy(JobVacancy jobVacancy) {
        jobVacancyRepo.delete(jobVacancy);
    }

    public void deleteJobVacancy(Integer id) {
        jobVacancyRepo.delete(id);
    }

    public List<JobVacancy> getAllJobVacancy( Map<String, String> params) {
        Predicate deptMatched = isNullOrEmpty(params.get("dept")) ? null : jobVacancy.job().department.eq(Department.valueOf(params.get("dept")));
        Predicate activeMatched = isNullOrEmpty(params.get("active")) ? null : jobVacancy.active.eq(Boolean.valueOf(params.get("active")));
        Predicate jobNameMatched = isNullOrEmpty(params.get("jobname")) ? null : jobVacancy.job().name.likeIgnoreCase(params.get("jobname"));
        Predicate jobIdMatched = isNullOrEmpty(params.get("id")) ? null : jobVacancy.id.eq(Integer.parseInt(params.get("id")));
        return newArrayList(jobVacancyRepo.findAll(anyOf(deptMatched, activeMatched, jobNameMatched, jobIdMatched)));
    }
}
