package com.abctech.hackathon.service;

import static com.abctech.hackathon.entity.QApplication.application;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.mysema.query.types.ExpressionUtils.anyOf;

import com.abctech.hackathon.entity.Application;
import com.abctech.hackathon.entity.ApplicationMessage;
import com.abctech.hackathon.entity.Department;
import com.abctech.hackathon.entity.JobLevel;
import com.abctech.hackathon.persistence.repository.ApplicationRepository;
import com.mysema.query.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author b0nyb0y
 */
@Service
@Transactional
public class ApplicationService {

    @Autowired
    private ApplicationRepository applicationRepo;

    public Application createApplication(Application application) {
        addInitMessage(application);
        return applicationRepo.save(application);
    }

    private void addInitMessage(Application parent) {
        ApplicationMessage result = new ApplicationMessage();
        result.setApplication(parent);
        result.setComment("created");
        result.setLastModified(LocalDateTime.now());
        result.setState(parent.getState());
        parent.setMessages(new ArrayList<>());
        parent.getMessages().add(result);
    }

    public Application updateApplication(Application application) {
        Application applicationSaved = applicationRepo.save(application);
        return applicationSaved;
     }

    public List<Application> findApplicationByCriteria(Map<String, String> params) {
        return newArrayList(applicationRepo.findAll());
    }

    public List<Application> findApplicationByApplicant(Map<String, String> params) {
        Predicate firstNameMatched = isNullOrEmpty(params.get("firstname")) ? null : application.applicant().firstName.likeIgnoreCase(params.get("firstname"));
        Predicate lastNameMatched = isNullOrEmpty(params.get("lastname")) ? null : application.applicant().lastName.likeIgnoreCase(params.get("lastname"));
        Predicate emailMatched = isNullOrEmpty(params.get("email")) ? null : application.applicant().email.equalsIgnoreCase(params.get("email"));
        return newArrayList(applicationRepo.findAll(anyOf(firstNameMatched, lastNameMatched, emailMatched)));
    }

    public List<Application> findApplicationByJob(Map<String, String> params) {
        Predicate deptMatched = isNullOrEmpty(params.get("dept")) ? null : application.vacancy().job().department.eq(Department.valueOf(params.get("dept")));
        Predicate jobNameMatched = isNullOrEmpty(params.get("jobname")) ? null : application.vacancy().job().name.likeIgnoreCase(params.get("jobname"));
        Predicate jLevelMatched = isNullOrEmpty(params.get("jlevel")) ? null : application.vacancy().job().level.eq(JobLevel.valueOf(params.get("jlevel")));
        return newArrayList(applicationRepo.findAll(anyOf(deptMatched, jobNameMatched, jLevelMatched)));
    }

    public Application getApplicationById(int id) {
        return applicationRepo.findOne(id);
    }
}
