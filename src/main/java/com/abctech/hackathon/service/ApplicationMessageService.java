package com.abctech.hackathon.service;

import com.abctech.hackathon.entity.Application;
import com.abctech.hackathon.entity.ApplicationMessage;
import com.abctech.hackathon.persistence.repository.ApplicationMessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.abctech.hackathon.entity.QApplicationMessage.applicationMessage;
import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by Tai on 12/29/2015.
 */
@Service
@Transactional
public class ApplicationMessageService {
    @Autowired
    private ApplicationMessageRepository applicationMsgRepo;

    public ApplicationMessage createOrUpdate(ApplicationMessage applicationMsg){
        return applicationMsgRepo.save(applicationMsg);
    }

    public List<ApplicationMessage> getAllApplicationMsgByApplication(Application application){
        return newArrayList(applicationMsgRepo.findAll(applicationMessage.application().eq(application)));
    }
}