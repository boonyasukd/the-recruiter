package com.abctech.hackathon.service;

import static com.abctech.hackathon.entity.QJob.job;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.mysema.query.types.ExpressionUtils.allOf;

import com.abctech.hackathon.entity.Department;
import com.abctech.hackathon.entity.Job;
import com.abctech.hackathon.entity.JobLevel;
import com.abctech.hackathon.persistence.repository.JobRepository;
import java.util.List;
import java.util.Map;

import com.mysema.query.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author b0nyb0y
 */
@Service
@Transactional
public class JobService {

    @Autowired
    private JobRepository jobRepo;

    public Job createOrUpdateJob(Job job) {
        return jobRepo.saveAndFlush(job);
    }

    public void deleteJob(Job job) {
        jobRepo.delete(job);
    }

    public void deleteJob(Integer id) {
        jobRepo.delete(id);
    }

    public List<Job> getJobs(Map<String, String> params) {
        Predicate deptMatched = isNullOrEmpty(params.get("dept")) ? null : job.department.eq(Department.valueOf(params.get("dept")));
        Predicate jobNameMatched = isNullOrEmpty(params.get("jobname")) ? null : job.name.likeIgnoreCase(params.get("jobname"));
        Predicate jLevelMatched = isNullOrEmpty(params.get("jlevel")) ? null : job.level.eq(JobLevel.valueOf(params.get("jlevel")));
        return newArrayList(jobRepo.findAll(allOf(deptMatched, jobNameMatched, jLevelMatched)));
    }

    public Job getJobById(int id) {
        return jobRepo.findOne(id);
    }
}
