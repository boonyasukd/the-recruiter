package com.abctech.hackathon.service;

import static com.abctech.hackathon.entity.QApplicant.applicant;
import static com.google.common.base.Strings.isNullOrEmpty;
import static com.google.common.collect.Lists.newArrayList;
import static com.mysema.query.types.ExpressionUtils.anyOf;

import com.abctech.hackathon.entity.Applicant;
import com.abctech.hackathon.persistence.repository.ApplicantRepository;
import com.mysema.query.types.Predicate;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author b0nyb0y
 */
@Service
@Transactional
public class ApplicantService {

    @Autowired
    private ApplicantRepository applicantRepo;

    public Applicant createOrUpdateApplicant(Applicant applicant) {
        return applicantRepo.saveAndFlush(applicant);
    }

    public List<Applicant> findApplicantsByCriteria(Map<String, String> params) {
        Predicate firstNameMatched = isNullOrEmpty(params.get("firstname")) ? null : applicant.firstName.likeIgnoreCase(params.get("firstname"));
        Predicate lastNameMatched = isNullOrEmpty(params.get("lastName")) ? null : applicant.lastName.likeIgnoreCase(params.get("lastName"));
        Predicate emailMatched = isNullOrEmpty(params.get("email")) ? null : applicant.email.equalsIgnoreCase(params.get("email"));
        return newArrayList(applicantRepo.findAll(anyOf(firstNameMatched, lastNameMatched, emailMatched)));
    }

    public void deleteApplicant(Applicant applicant) {
        applicantRepo.delete(applicant);
    }

    public void deleteApplicant(Integer id) {
        applicantRepo.delete(id);
    }
}
