package com.abctech.hackathon;

import java.util.LinkedHashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.JpaBaseConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.orm.jpa.vendor.AbstractJpaVendorAdapter;
import org.springframework.orm.jpa.vendor.EclipseLinkJpaVendorAdapter;

@SpringBootApplication
public class RecruiterServerApplication extends JpaBaseConfiguration {
    private static final Logger log = LoggerFactory.getLogger(RecruiterServerApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(RecruiterServerApplication.class, args);
    }

    @Autowired
    private JpaProperties jpaProperties;

    @Override
    protected AbstractJpaVendorAdapter createJpaVendorAdapter() {
        return new EclipseLinkJpaVendorAdapter();
    }

    @Override
    protected Map<String, Object> getVendorProperties() {
        jpaProperties.getProperties().forEach((key, value) -> log.debug("key: {} -> {}", key, value));
        return new LinkedHashMap<>(jpaProperties.getProperties());
    }
}
